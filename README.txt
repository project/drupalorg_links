Drupal.org Links
----------------
A collection of field formatters for converting various types of values into
links to drupal.org.


Features
--------------------------------------------------------------------------------
The primary features include:

* Comment link - converts a number to a link in the format
  "https://www.drupal.org/comment/[VALUE]". Supports integer, decimal and string
  values, takes the integer value of the number.

* Node link - converts a number to a link in the format
  "https://www.drupal.org/node/[VALUE]". Supports integer, decimal and string
  values, takes the integer value of the number.

* User link - converts a number to a link in the format
  "https://www.drupal.org/user/[VALUE]". Supports integer, decimal and string
  values, takes the integer value of the number.


Credits / contact
--------------------------------------------------------------------------------
Written and maintained by Damien McKenna [1]

Ongoing development is sponsored by Mediacurrent [2].

The best way to contact the author(s) is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  https://www.drupal.org/project/issues/drupalorg_links


References
--------------------------------------------------------------------------------
1: https://www.drupal.org/u/damienmckenna
2: https://www.mediacurrent.com/
